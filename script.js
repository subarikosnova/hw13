// 1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування
// Механизм который позволяет использовать спец символы . что бы отделить функциональные символы от самого символа , наприме:  \n (символ нового ряда)
// 2. Які засоби оголошення функцій ви знаєте?
// _____ Стандартное: function _____
// function addNumbers(a, b) { return a + b; }
// _____ Стрелочная: => _____
// const addNumbers = (a, b) => a + b;
// _____ Анонимная _____
// const multiplyNumbers = function(a, b) { return a * b; };
// _____ Функции в классах _____
// class Calculator { addNumbers(a, b) { return a + b; } }
// 3. Що таке hoisting, як він працює для змінних та функцій?
// Простыми словами поднятия , описывает спецефическое поведение во время фазы обрабтки кода.
// Функции, объявленные с помощью ключевого слова function, возвышаются полностью – как их имя, так и тело функции. Это позволяет обращаться к функциям даже для их объявления.
// Во время hoisting переменные, объявленные с помощью ключевого слова var, переносятся в начало своей области видимости (функции или глобальной области видимости), но их инициализация остается на месте.
function createNewUser() {
    const newUser = {};
    newUser.firstName = prompt("Введіть ім'я:");
    newUser.lastName = prompt("Введіть прізвище:");
    newUser.birthday = prompt("Введіть дату народження (у форматі dd.mm.yyyy):");

    newUser.getLogin = function() {
        return (this.firstName.charAt(0) + this.lastName).toLowerCase();
    };

    newUser.getAge = function() {
        const today = new Date();
        const birthDate = new Date(this.birthday);
        let age = today.getFullYear() - birthDate.getFullYear();
        const monthDiff = today.getMonth() - birthDate.getMonth();
        if (monthDiff < 0 || (monthDiff === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    };

    newUser.getPassword = function() {
        const firstLetter = this.firstName.charAt(0).toUpperCase();
        const lastNameLower = this.lastName.toLowerCase();
        const year = this.birthday.split(".")[2];
        return firstLetter + lastNameLower + year;
    };

    return newUser;
}

const user = createNewUser();
console.log("Користувач:", user.firstName, user.lastName);
console.log("Логін користувача:", user.getLogin());
console.log("Вік користувача:", user.getAge(), "років");
console.log("Пароль користувача:", user.getPassword());
